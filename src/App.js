import React, {useEffect, useState} from 'react';
import './App.css';

function App() {
    const [day, setDay] = useState('00');
    const [hour, setHour] = useState('00');
    const [minutes, setMinutes] = useState('00');
    const [seconds, setSeconds] = useState('00');
    const [input, setInput] = useState();
    const [value, setValue] = useState(0);

    let Timer;
    const click = () => {
        const x = input.toString();
        setValue(x)
        return x
    }
    const start = () => {
        const countDownDate = new Date(value).getTime()
        Timer = setInterval(() => {

            const now = new Date().getTime();
            const distance = countDownDate - now;
            const Days = Math.floor(distance / (24 * 60 * 60 * 1000));
            const Hours = Math.floor((distance % (24 * 60 * 60 * 1000) / (1000 * 60 * 60)));
            const Minutes = Math.floor((distance % (60 * 60 * 1000)) / (1000 * 60));
            const Seconds = Math.floor((distance % (60 * 1000)) / 1000);
            if (distance < 0) {
                clearInterval(Timer.current)
            } else {
                setDay(Days.fixFormat())
                setHour(Hours.fixFormat())
                setMinutes(Minutes.fixFormat())
                setSeconds(Seconds.fixFormat())
            }

        }, 1000)
    };
    useEffect(() => {
        start()
        return () => {
            clearInterval(Timer.current)
        }
    })
    Number.prototype.fixFormat = function () {
        const target = this.toString();
        return (target.length === 1) ? '0' + target : target;
    }
    return (
        <div className="App">
            <div className="Container">
                <p style={{color: "white", fontSize: '20px'}}>برای مثال اگر تاریخ امروزسیزدهم جولای 2021 بود شما تاریخ
                    مثلا پانزدهم جولای 2021 را وارد نمائید </p>
                <div>
                    <label style={{color: "white", fontSize: '25px'}}>Date : </label>
                    <input style={{padding: "10px", borderRadius: '10px',margin:"10px"}} type="text" onChange={(e) => setInput(e.target.value)}/>
                    <button style={{padding: "5px 15px",border: "none", fontSize: '20px',backgroundColor:"red", borderRadius: '10px'}} onClick={click}>click</button>
                </div>
                <div className="Timer">
                    <span className="day">{day}</span>
                    <span className="separate">:</span>
                    <span className="hour">{hour}</span>
                    <span className="separate">:</span>
                    <span className="Minutes">{minutes}</span>
                    <span className="separate">:</span>
                    <span className="Seconds">{seconds}</span>
                </div>
            </div>
        </div>
    );
}

export default App;
